#!/bin/bash


function pushd {
    command pushd "$@" > /dev/null
}

function popd {
    command popd > /dev/null
}

function find_path {
    local path="$1"
    local real_path=""
    if [ -e "${path}" ]; then
        real_path=$(readlink -e "${path}")
    fi
    echo "${real_path}"
}

function check_local_changes {
    local repo_path="$1" && shift
    local projects=("$@")

    for project in "${projects[@]}"; do
        pushd "${repo_path}/${project}"
        if ! git diff-index --quiet HEAD; then
            error_exit "Local changes detected in: ${project}"
        fi
        popd
    done
}

function warning {
    local warning="$1"
    printf "\033[0;33mWARNING:\033[0m ${warning}\n\n"
}

function error {
    local error="$1"
    printf "\033[0;31mERROR:\033[0m ${error}\n\n"
}

function error_exit {
    error "$1"
    exit 1
}

function error_usage_exit {
    error "$1"
    usage
    exit 1
}

