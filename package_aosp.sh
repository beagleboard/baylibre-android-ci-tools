#!/bin/bash

set -e
set -o pipefail

SRC=$(dirname "$(readlink -e "$0")")

source "${SRC}/utils.sh"

function usage {
    cat <<DELIM__
Usage: package_aosp.sh <options>
    options:
        --help Show this message and exit
        --out-dir: android's OUT_DIR where the files are collected from
        --name: package output name
        --extra-target-files: space separated list of out/target/product/* files to be added
        --extra-host-files: space separated list of host/linux-x86/bin/* files to be added
        --ignore-missing: don't error if we can't copy some files (useful for failed builds)
DELIM__
}

function package_aosp {
    local outdir="$1"
    local name="$2"
    local extra_target_files="$3"
    local extra_host_files="$4"
    local ignore_missing="$5"
    local package_tempdir="${PWD}/${name}"
    local package_fullpath="${PWD}/package/${name}.tar.gz"

    mkdir --parents "${package_tempdir}"
    mkdir --parents "$(dirname ${package_fullpath})"

    # collect images/files and put them in dest folder
    echo "[*] copying log files files"
    cp ${outdir}/*.log "${package_tempdir}"

    standard_android_images=(
        boot
        userdata
        vbmeta
        super
        vendor_boot
        init_boot
    )

    echo "[*] copying images: $images"
    for image in ${standard_android_images[@]}; do
        local src_image=${outdir}/target/product/*/${image}.img
        if [ -f $src_image ]; then
            echo "    copying ${image}.img"
            cp $src_image "${package_tempdir}"
        else
            warning "${image}.img not found, not copying"
        fi
    done

    echo "[*] copying extra target files: $extra_target_files"
    for f in ${extra_target_files}; do
        if [[ "$ignore_missing" == true ]] && [ ! -f ${outdir}/target/product/*/${f} ]; then
            warning "${f} not found, not copying"
        else
            cp ${outdir}/target/product/*/${f} "${package_tempdir}"
        fi
    done

    echo "[*] copying base host tools"
    for f in adb fastboot; do
        if [[ "$ignore_missing" == true ]] && [ ! -f ${outdir}/host/linux-x86/bin/${f} ]; then
            warning "${f} not found, not copying"
        else
            cp ${outdir}/host/linux-x86/bin/${f} "${package_tempdir}"
        fi
    done

    echo "[*] copying extra host tools: $extra_host_files"
    for f in  ${extra_host_files}; do
        if [[ "$ignore_missing" == true ]] && [ ! -f ${outdir}/host/linux-x86/bin/${f} ]; then
            warning "${f} not found, not copying"
        else
            cp ${outdir}/host/linux-x86/bin/${f} "${package_tempdir}"
        fi
    done

    echo "[*] packaging in ${package_fullpath} ..."
    tar --create --auto-compress --remove-files --file "${package_fullpath}" -C "${package_tempdir}"/.. "$(basename ${package_tempdir})"
    echo "[*] packaged under ${package_fullpath}"
}

function main {
    local script=$(basename "$0")
    local outdir=""
    local name=""
    local extra_target_files=""
    local extra_host_files=""
    local ignore_missing=false

    local opts_args="out-dir:,name:,extra-target-files:,extra-host-files:,ignore-missing,help"
    local opts=$(getopt -o '' -l "${opts_args}" -- "$@")
    eval set -- "${opts}"

    while true; do
        case "$1" in
            --out-dir) outdir="$2"; shift 2 ;;
            --name) name="$2"; shift 2 ;;
            --extra-target-files) extra_target_files="$2"; shift 2 ;;
            --extra-host-files) extra_host_files="$2"; shift 2;;
            --ignore-missing) ignore_missing=true; shift ;;
            --help) usage; exit 0 ;;
            --) shift; break ;;
        esac
    done

    # check arguments
    [ -z "${outdir}" ] &&  error_usage_exit "No out_dir provided"
    [ -z "${name}" ] &&  error_usage_exit "No name provided"

    package_aosp "${outdir}" "${name}" "${extra_target_files}" "${extra_host_files}" "${ignore_missing}"
    exit 0
}

if [ "$0" = "$BASH_SOURCE" ]; then
    main "$@"
fi
