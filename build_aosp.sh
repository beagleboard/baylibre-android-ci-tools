#!/bin/bash

set -e
set -o pipefail

SRC=$(dirname "$(readlink -e "$0")")

source "${SRC}/utils.sh"

function usage {
    cat <<DELIM__
Usage: build_aosp.sh <options>
    options:
        --help Show this message and exit
        --target: entry in lunch menu
        --build-args: give specific arg to build
        --out-dir (optional): custom out dir for android, will be created next in ${PWD}
        --aosp-path: android source tree root folder
        --ncores: to specific number of CPU used to build
DELIM__
}

function build_aosp {
    local target="${1}"
    local outdir="${2}"
    local buildargs="${3}"
    local aosppath="${4}"
    local ncores="${5:-}"
    local cmdline=""

    if ! [ -z "${ncores}" ]; then
        cmdline="-j ${ncores} "
    fi
    cmdline+=${buildargs}

    if [ ! -z ${outdir} ]; then
        outdir=$(realpath ${outdir})
        export OUT_DIR=${outdir}
    fi

    pushd ${aosppath}

    source ./build/envsetup.sh
    lunch ${target}
    make ${cmdline}
    popd

    unset OUT_DIR
}

function main {
    local script=$(basename "$0")
    local aosppath=""
    local ncores=""
    local target=""
    local buildargs=""
    local outdir=""
    local log_path=""

    local opts_args="ncores:,aosp-path:,target:,build-args:,out-dir:,help"
    local opts=$(getopt -o '' -l "${opts_args}" -- "$@")
    eval set -- "${opts}"

    while true; do
        case "$1" in
            --ncores) ncores="$2"; shift 2 ;;
            --aosp-path) aosppath="$2"; shift 2 ;;
            --target) target="$2"; shift 2 ;;
            --build-args) buildargs="$2"; shift 2 ;;
            --out-dir) outdir="$2"; shift 2 ;;
            --help) usage; exit 0 ;;
            --) shift; break ;;
        esac
    done

    # check arguments
    [ -z "${target}" ] &&  error_usage_exit "Not lunch target provided"
    [ -z "${aosppath}" ] &&  error_usage_exit "Not aosp env path provided"

    if [ -z ${outdir} ]; then
        log_path="${aosppath}/out/${target}.log"
    else
        log_path="${outdir}/${target}.log"
    fi

    echo "Build logs will be stored at: [ $log_path ]"
    exec >${log_path} 2>&1

    build_aosp "${target}" "${outdir}" "${buildargs}" "${aosppath}" "${ncores}"
    exit 0
}

if [ "$0" = "$BASH_SOURCE" ]; then
    main "$@"
fi